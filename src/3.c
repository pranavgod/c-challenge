
// Level 3
#include<stdio.h>

int main(int argc, char* argv[]){
  if(argc != 2){
    fprintf(stderr, "Only 1 argument please. It must be a number.");
    exit(1);
  }

  int i = atoi(argv[1]);
  for(; i > 0; i--){
    printf("PON");
  }
  return 0;
}
