
// Level 1
#include<stdio.h>

int main(int argc, char* argv[]){
  if(argc != 2){
    fprintf(stderr, "Incorrect number of arguments. Please do ./1 [Any String]");
    exit(1);
  }else{
    printf("You entered: %s", argv[1]);
  }
  return 0;
}
