// Level 4
#include<stdio.h>
#define MAX 1000

int main(int argc, char* argv[]){
  char* acc = calloc(MAX, sizeof(char));
  printf("Please enter a string: ");
  int i = 0;
  char c = 0;
  while((c = getchar()) != '\n'){
    acc[i] = c;
    i++;
  }
  acc[i] = 0;
  printf("You entered: %s", acc);
  return 0;

}
