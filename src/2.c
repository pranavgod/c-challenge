
// Level 2
#include<stdio.h>

int main(int argc, char* argv[]){
  if(argc < 2){
    fprintf(stderr, "Incorrect number of arguments. You must have at least one argument: ./2 [...Any number of arguments...]");
    exit(1);
  }
  

  int i;
  for(i=1; i < argc; i++){
    printf("%s ", argv[i]);
  }
  return 0;
    
}
